package com.gojek.beam.interview.ptransform.grouping;

import com.gojek.beam.interview.models.GoBase;
import org.apache.beam.sdk.transforms.PTransform;
import org.apache.beam.sdk.values.KV;
import org.apache.beam.sdk.values.PCollection;

/**
 * Created by kevin.sutardji on 8/10/18.
 */
public class GroupByBookingIdAsKey extends PTransform<PCollection<GoBase>, PCollection<KV<String, Iterable<GoBase>>>> {

    @Override
    public PCollection<KV<String, Iterable<GoBase>>> expand(PCollection<GoBase> kvpCollection) {
        return null;
    }
}
