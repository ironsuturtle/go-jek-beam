package com.gojek.beam.interview.function;

import com.gojek.beam.interview.models.GoBase;
import com.google.common.collect.Iterables;
import org.apache.beam.sdk.metrics.Counter;
import org.apache.beam.sdk.metrics.Metrics;
import org.apache.beam.sdk.transforms.DoFn;
import org.apache.beam.sdk.values.KV;

/**
 * Counter for number of Keys
 */
public class BookingCounter extends DoFn<KV<String, Iterable<GoBase>>, Void> {

    private static final Counter elementCounter = Metrics.counter(BookingCounter.class,"elementCounter");


    @ProcessElement
    public void processElement(ProcessContext processContext) {
        KV<String, Iterable<GoBase>> element = processContext.element();
        if(Iterables.size(element.getValue()) > 0) {
            elementCounter.inc();
        }
    }
}
