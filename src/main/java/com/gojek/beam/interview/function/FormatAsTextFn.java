package com.gojek.beam.interview.function;

import com.gojek.beam.interview.models.GoBase;
import com.gojek.beam.interview.models.GoBaseModel;
import org.apache.beam.sdk.metrics.Counter;
import org.apache.beam.sdk.metrics.Metrics;
import org.apache.beam.sdk.transforms.SimpleFunction;

/** A SimpleFunction that converts a Word and Count into a printable string. */
public class FormatAsTextFn extends SimpleFunction<GoBase, String> {
    @Override
    public String apply(GoBase input) {
        return input.toString();
    }
}
