package com.gojek.beam.interview.function;

import com.gojek.beam.interview.models.GoBase;
import com.gojek.beam.interview.models.GoBaseModel;
import com.gojek.beam.interview.models.GoInterviewModel;
import com.gojek.beam.interview.models.common.ServiceType;
import com.gojek.beam.interview.models.GoRideModel;
import org.apache.beam.sdk.metrics.Counter;
import org.apache.beam.sdk.metrics.Metrics;
import org.apache.beam.sdk.transforms.DoFn;
import org.apache.beam.sdk.transforms.windowing.BoundedWindow;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ParseJsonFn extends DoFn<String, GoBase> {
    private static final Logger LOG = LoggerFactory.getLogger(ParseJsonFn.class);
    private static final Counter gojekBookingsCounter = Metrics.counter(ParseJsonFn.class, "gojekBookings");
    private static final Counter failedParsingCounter = Metrics.counter(ParseJsonFn.class, "failedParsing");

    public ParseJsonFn() {

    }

    @ProcessElement
    public void processElement(ProcessContext processContext, BoundedWindow window) {
        // element here is your line, you can whatever you want, parse, print, etc
        // this function will be simply applied to all elements in your stream
        String element = processContext.element();
        try {
            JSONObject jsonObject = new JSONObject(element);
            GoBase goBase = createModelFromJson(jsonObject);
            this.gojekBookingsCounter.inc();
            if(goBase.getTimestamp() == null) {
                throw new Exception();
            }
            processContext.outputWithTimestamp(goBase, goBase.getTimestamp());
        } catch(Exception e) {
            failedParsingCounter.inc();
            LOG.error("Parse error on element: " + element + ", " + e.getMessage());
            sendElementToDLQ(element, processContext);
        }
    }

    private GoBase createModelFromJson(JSONObject jsonObject) {
        if(jsonObject.has("service_type")) {
            ServiceType serviceType = jsonObject.getEnum(ServiceType.class, "service_type");
            switch(serviceType) {
                case GO_RIDE:
                    return new GoRideModel(jsonObject);
                case GO_INTERVIEW:
                    return new GoInterviewModel(jsonObject);
                default:
                    throw new UnsupportedOperationException("Current model is not supported in the Data Pipeline: " + serviceType.toString());
            }
        }
        return null;
    }

    private void sendElementToDLQ(@Element String element, ProcessContext processContext) {
        // send element to Dead Letter Queue to either re-process or to debug
    }
}

