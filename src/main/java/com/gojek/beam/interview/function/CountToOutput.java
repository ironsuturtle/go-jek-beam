package com.gojek.beam.interview.function;

import com.gojek.beam.interview.models.GoBase;
import org.apache.beam.sdk.transforms.DoFn;
import org.apache.beam.sdk.values.PCollection;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CountToOutput extends DoFn<PCollection<Long>, String> {
    private static final Logger LOG = LoggerFactory.getLogger(CountToOutput.class);

    @ProcessElement
    public String processElement(ProcessContext processContext) {
        return "";
    }
}
