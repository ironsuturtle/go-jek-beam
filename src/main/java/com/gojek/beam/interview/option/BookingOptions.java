package com.gojek.beam.interview.option;


import org.apache.beam.sdk.options.Default;
import org.apache.beam.sdk.options.Description;

/**
 * Booking Options for Go-Jek Booking Data Pipeline
 */
public interface BookingOptions extends OptionsBase {
    @Description("Output type for data pipeline results")
    @Default.String("text")
    String getOutputType();
    void setOutputType(String outputType);

    @Description("Output type for count of bookings per window")
    @Default.String("count")
    String getCountOutputType();
    void setCountOutputType(String countOutputType);

    @Description("Fixed window duration, in minutes")
    Integer getWindowSize();
    void setWindowSize(Integer value);

    @Description("Fixed number of shards to produce per window")
    Integer getNumShards();
    void setNumShards(Integer numShards);
}
