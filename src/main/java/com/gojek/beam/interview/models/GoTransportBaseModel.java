package com.gojek.beam.interview.models;

import org.json.JSONObject;

/**
 * Go-Jek Transportation model abstract class (for Go-Ride, Go-Food, Go-Car, etc.)
 */
public abstract class GoTransportBaseModel extends GoBaseModel {
    private String driverId;
    private String customerId;

    GoTransportBaseModel(JSONObject jsonObject) {
        super(jsonObject);
        this.driverId = jsonObject.getString("driver_id");
        this.customerId = jsonObject.getString("customer_id");
    }

    public String getDriverId() {
        return driverId;
    }

    public String getCustomerId() {
        return customerId;
    }

    @Override
    public String toString() {
      return String.format("%s,%s,%s", super.toString(), driverId, customerId);
    }
}
