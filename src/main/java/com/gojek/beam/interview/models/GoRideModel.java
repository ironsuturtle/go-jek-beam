package com.gojek.beam.interview.models;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Represents a model for Go-Ride data
 */
public class GoRideModel extends GoTransportBaseModel {

    List<String> otherPassengerIds = new ArrayList<>();

    public GoRideModel(JSONObject jsonObject) {
        super(jsonObject);
        JSONArray interviewerArray = jsonObject.getJSONArray("other_passenger_ids");
        for(int i = 0; i < interviewerArray.length(); i++) {
            addOtherPassengerId(interviewerArray.getString(i));
        }
    }

    public List<String> getOtherPassengerIds() {
        return otherPassengerIds;
    }

    public GoRideModel addOtherPassengerId(String otherPassengerId) {
        this.otherPassengerIds.add(otherPassengerId);
        return this;
    }

    @Override
    public String toString() {
        return String.format("%s,%s", super.toString(), Arrays.toString(otherPassengerIds.toArray()));
    }

}
