package com.gojek.beam.interview.models;

import com.gojek.beam.interview.models.common.ServiceType;
import com.gojek.beam.interview.models.common.BookingStatus;
import com.gojek.beam.interview.models.common.PaymentType;
import org.joda.time.Instant;
import org.json.JSONObject;

/**
 * Abstract base class for all Go-Jek services model
 */
public abstract class GoBaseModel implements GoBase {

    private final String orderNumber;
    private final ServiceType serviceType;
    private final String serviceAreaName;
    private final PaymentType paymentType;
    private final BookingStatus bookingStatus;
    private final Instant timestamp;

    GoBaseModel(final JSONObject jsonObject) {
        this.orderNumber = jsonObject.getString("order_number");
        this.serviceType = jsonObject.getEnum(ServiceType.class, "service_type");
        this.serviceAreaName = jsonObject.getString("service_area_name");
        this.paymentType = jsonObject.getEnum(PaymentType.class, "payment_type");
        this.bookingStatus = jsonObject.getEnum(BookingStatus.class, "status");
        this.timestamp = Instant.parse(jsonObject.getString("event_timestamp"));
    }

    @Override
    public String getOrderNumber() {
        return this.orderNumber;
    }

    @Override
    public ServiceType getServiceType() {
        return this.serviceType;
    }

    @Override
    public String getServiceAreaName() {
        return serviceAreaName;
    }

    @Override
    public BookingStatus getBookingStatus() {
        return this.bookingStatus;
    }

    @Override
    public PaymentType getPaymentType() {
        return this.paymentType;
    }

    @Override
    public Instant getTimestamp() {
        return this.timestamp;
    }

    @Override
    public String toString() {
        return String.format("%s,%s,%s,%s,%s,%s", getOrderNumber(), getServiceType(), getServiceAreaName(),
                getPaymentType(), getBookingStatus(), getTimestamp());
    }
}
