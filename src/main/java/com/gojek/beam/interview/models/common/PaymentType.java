package com.gojek.beam.interview.models.common;

/**
 * Enums of accepted payments for Go-Jek services
 */
public enum PaymentType {
    GO_CASH, CASH, CHECK, CREDIT_CARD, DEBIT_CARD
}
