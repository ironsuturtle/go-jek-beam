package com.gojek.beam.interview.models.common;

/**
 * Booking statuses for Go-Jek services
 */
public enum BookingStatus {
    REQUESTED, IN_PROGRESS, COMPLETED, CANCELLED, FAILED
}
