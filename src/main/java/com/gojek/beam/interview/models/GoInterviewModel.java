package com.gojek.beam.interview.models;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Represents a model for Go-Interview model
 */
public class GoInterviewModel extends GoBaseModel {
    private List<String> interviewerIds = new ArrayList<>();
    private String intervieweeId;

    public GoInterviewModel(JSONObject jsonObject) {
        super(jsonObject);
        this.intervieweeId = jsonObject.getString("interviewee_id");
        JSONArray interviewerArray = jsonObject.getJSONArray("interviewer_ids");
        for(int i = 0; i < interviewerArray.length(); i++) {
            addInterviewerId(interviewerArray.getString(i));
        }
    }

    public void addInterviewerId(String interviewerId) {
        this.interviewerIds.add(interviewerId);
    }

    @Override
    public String toString() {
        return String.format("%s,%s,%s", super.toString(), Arrays.toString(interviewerIds.toArray()), intervieweeId);
    }
}
