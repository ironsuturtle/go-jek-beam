package com.gojek.beam.interview.models;

import com.gojek.beam.interview.models.common.BookingStatus;
import com.gojek.beam.interview.models.common.PaymentType;
import com.gojek.beam.interview.models.common.ServiceType;
import org.joda.time.Instant;

import java.io.Serializable;

/**
 * Interface that contains methods that are required for all Go-Jek services
 */
public interface GoBase extends Serializable {

    /**
     * returns the order number of GoX, this ID should be unique
     * @return
     */
    String getOrderNumber();

    /**
     * Returns the service type of the service (i.e. GO_INTERVEW, GO_RIDE, GO_FOOD, etc.)
     * @return
     */
    ServiceType getServiceType();

    /**
     * Returns the name of the area the service took place
     * @return
     */
    String getServiceAreaName();

    /**
     * Returns the payment type of the order (i.e. GO_CASH, CASH, CHECK, CREDIT_CARD, etc.)
     * @return
     */
    PaymentType getPaymentType();

    /**
     * Returns the status of the order (i.e. REQUESTED, IN_PROGRESS, COMPLETED)
     * @return
     */
    BookingStatus getBookingStatus();

    /**
     * Returns the event timestamp, the time when the event happened
     * @return
     */
    Instant getTimestamp();

    /**
     * Default method to return the order number as the group by key
     * @return
     */
    default String getKey() {
        return getOrderNumber();
    }
}
