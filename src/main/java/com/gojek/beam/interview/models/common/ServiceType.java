package com.gojek.beam.interview.models.common;

import com.google.gson.annotations.SerializedName;

/**
 * Enums of Go-Jek services
 */
public enum ServiceType {
    @SerializedName("GO_RIDE") GO_RIDE,

    @SerializedName("GO_CAR") GO_CAR,

    @SerializedName("GO_FOOD") GO_FOOD,

    @SerializedName("GO_SEND") GO_SEND,

    @SerializedName("GO_BOX") GO_BOX,

    @SerializedName("GO_TIX") GO_TIX,

    @SerializedName("GO_MED") GO_MED,

    @SerializedName("GO_INTERVIEW") GO_INTERVIEW
}
