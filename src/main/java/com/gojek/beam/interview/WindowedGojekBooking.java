package com.gojek.beam.interview;

import com.gojek.beam.interview.function.BookingCounter;
import com.gojek.beam.interview.function.FormatAsTextFn;
import com.gojek.beam.interview.function.ParseJsonFn;
import com.gojek.beam.interview.models.GoBase;
import com.gojek.beam.interview.option.BookingOptions;
import com.gojek.beam.interview.ptransform.output.GoOutput;
import org.apache.beam.sdk.Pipeline;
import org.apache.beam.sdk.PipelineResult;
import org.apache.beam.sdk.io.TextIO;
import org.apache.beam.sdk.options.PipelineOptionsFactory;
import org.apache.beam.sdk.transforms.Combine;
import org.apache.beam.sdk.transforms.Count;
import org.apache.beam.sdk.transforms.GroupByKey;
import org.apache.beam.sdk.transforms.MapElements;
import org.apache.beam.sdk.transforms.ParDo;
import org.apache.beam.sdk.transforms.Sum;
import org.apache.beam.sdk.transforms.WithTimestamps;
import org.apache.beam.sdk.transforms.windowing.AfterPane;
import org.apache.beam.sdk.transforms.windowing.AfterProcessingTime;
import org.apache.beam.sdk.transforms.windowing.AfterWatermark;
import org.apache.beam.sdk.transforms.windowing.SlidingWindows;
import org.apache.beam.sdk.transforms.windowing.Trigger;
import org.apache.beam.sdk.transforms.windowing.Window;
import org.apache.beam.sdk.values.KV;
import org.apache.beam.sdk.values.PCollection;
import org.apache.beam.sdk.values.TypeDescriptor;
import org.apache.beam.sdk.values.TypeDescriptors;
import org.joda.time.Duration;
import org.joda.time.Instant;

import java.io.IOException;

/**
 *
 */
public class WindowedGojekBooking {
    // default window size in minutes
    private static final int DEFAULT_WINDOW_SIZE = 60;
    private static final int DEFAULT_WINDOW_FREQUENCY = 5;
    private static final int DEFAULT_ALLOWED_LATENESS = 1;
    private static final String DEFAULT_PUBSUB_BOOKING_TOPIC = "BOOKING_TOPIC";

    static void runWindowedGoJekBookings(BookingOptions options) throws IOException {
        // set up output
        final GoOutput pOutput = GoOutput.createOutputTransform(options.getOutputType(), options.getOutput());

        // trigger for windowing - trigger past the end of the window
        // fire if any element is processed in the window pane
        // fire to fix any late firings right away
        final Trigger trigger = AfterWatermark.pastEndOfWindow()
                .withEarlyFirings(AfterProcessingTime.pastFirstElementInPane())
                .withLateFirings(AfterPane.elementCountAtLeast(1));

        // create pipeline from options
        Pipeline pipeline = Pipeline.create(options);

        // read input from pub-sub topic
        //PubsubIO.<String>readStrings().withTimestampAttribute("event_timestamp").fromTopic("DEFAULT_PUBSUB_BOOKING_TOPIC");

        // read input from input file
        PCollection<String> input = pipeline
                .apply(TextIO.read().from(options.getInputFile()));

        PCollection<GoBase> goBasePCollection = input.apply(ParDo.of(new ParseJsonFn()));

        // create windowed collection based on input collection
        PCollection<GoBase> windowedCollection =
                // We want to have 1 hour windows
                goBasePCollection
                        .apply(WithTimestamps.of((GoBase g) -> new Instant(g.getTimestamp())))
                        .apply(Window.<GoBase>into(SlidingWindows.of(Duration.standardMinutes(options.getWindowSize()))
                        .every(Duration.standardMinutes(DEFAULT_WINDOW_FREQUENCY)))
                    // with a 99.99% data quality in completeness
                    .triggering(trigger)
                    .withAllowedLateness(Duration.standardDays(DEFAULT_ALLOWED_LATENESS))
                    .accumulatingFiredPanes());

        // transform the GoBaseModels into the formatted output
        PCollection<String> outputCollection = windowedCollection
                .apply(MapElements.via(new FormatAsTextFn()));

        // write to output file
        outputCollection
                .apply(pOutput);

        //================ COUNT ================
        // group by key - order number
        PCollection<KV<String, GoBase>> goBaseMap = windowedCollection.apply(
                MapElements.into(
                        TypeDescriptors.kvs(TypeDescriptors.strings(), TypeDescriptor.of(GoBase.class)))
                        .via((GoBase gb) -> KV.of(gb.getKey(), gb)));

        // create collection of GoBaseModels
        PCollection<KV<String, Iterable<GoBase>>> groupedGoBases = goBaseMap.apply(GroupByKey.create());

        groupedGoBases.apply(ParDo.of(new BookingCounter()));
        // run pipeline and get results
        PipelineResult result = pipeline.run();
        try {
            result.waitUntilFinish();
        } catch (Exception exc) {
            result.cancel();
        }
    }

    public static void main(String[] args) throws IOException {

        BookingOptions options = PipelineOptionsFactory.fromArgs(args).withValidation().as(BookingOptions.class);
        options.setWindowSize(DEFAULT_WINDOW_SIZE);
        options.setOutputType("text");
        options.setCountOutputType("count");
        runWindowedGoJekBookings(options);
    }

}
