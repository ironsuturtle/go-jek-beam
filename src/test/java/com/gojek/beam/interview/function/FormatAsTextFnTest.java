package com.gojek.beam.interview.function;

import com.gojek.beam.interview.models.GoBase;
import com.gojek.beam.interview.models.GoRideModel;
import org.apache.beam.sdk.transforms.DoFnTester;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(JUnit4.class)
public class FormatAsTextFnTest {
    @Test
    public void testFormatAsTextFn() throws Exception {
        DoFnTester<String, GoBase> doFnTester = DoFnTester.of(new ParseJsonFn());

        List<GoBase> goBaseList = doFnTester.processBundle("{\"order_number\": \"AP-1\",\"service_type\": \"GO_RIDE\"," +
                "\"driver_id\": \"driver-123\",\"customer_id\": \"customer-123\",\"service_area_name\": \"JAKARTA\"," +
                "\"payment_type\": \"GO_CASH\",\"status\": \"COMPLETED\"," +
                "\"event_timestamp\": \"2018-03-29T14:00:00.001Z\", " +
                "\"other_passenger_ids\" : [\"passenger-123\", \"passenger-456\"]}");
        assertTrue(goBaseList.size() == 1);
        GoBase goBase = goBaseList.get(0);
        assertTrue(goBase instanceof GoRideModel);
        GoRideModel goRide = (GoRideModel) goBase;
        assertEquals("AP-1,GO_RIDE,JAKARTA,GO_CASH,COMPLETED,2018-03-29T14:00:00.001Z,driver-123,customer-123,[passenger-123, passenger-456]", goRide.toString());
    }
}
