package com.gojek.beam.interview.function;

import com.gojek.beam.interview.models.GoBase;
import com.gojek.beam.interview.models.GoRideModel;
import com.gojek.beam.interview.models.common.BookingStatus;
import com.gojek.beam.interview.models.common.PaymentType;
import com.gojek.beam.interview.models.common.ServiceType;
import org.apache.beam.sdk.transforms.DoFnTester;
import org.joda.time.Instant;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(JUnit4.class)
public class ParseJsonFnTest {

    @Test
    public void testParseJsonFnTest() throws Exception {
        DoFnTester<String, GoBase> doFnTester = DoFnTester.of(new ParseJsonFn());

        List<GoBase> goBaseList = doFnTester.processBundle("{\"order_number\": \"AP-1\",\"service_type\": \"GO_RIDE\"," +
                "\"driver_id\": \"driver-123\",\"customer_id\": \"customer-123\",\"service_area_name\": \"JAKARTA\"," +
                "\"payment_type\": \"GO_CASH\",\"status\": \"COMPLETED\"," +
                "\"event_timestamp\": \"2018-03-29T14:00:00.001Z\", " +
                "\"other_passenger_ids\" : [\"passenger-123\", \"passenger-456\"]}\n");
        assertTrue(goBaseList.size() == 1);
        GoBase goBase = goBaseList.get(0);
        assertTrue(goBase instanceof GoRideModel);
        GoRideModel goRide = (GoRideModel) goBase;
        assertEquals(goRide.getOrderNumber(), "AP-1");
        assertEquals(goRide.getServiceType(), ServiceType.GO_RIDE);
        assertEquals(goRide.getDriverId(), "driver-123");
        assertEquals(goRide.getCustomerId(), "customer-123");
        assertEquals(goRide.getServiceAreaName(), "JAKARTA");
        assertEquals(goRide.getPaymentType(), PaymentType.GO_CASH);
        assertEquals(goRide.getBookingStatus(), BookingStatus.COMPLETED);
        assertEquals(goRide.getTimestamp(), Instant.parse("2018-03-29T14:00:00.001Z"));
    }
}
