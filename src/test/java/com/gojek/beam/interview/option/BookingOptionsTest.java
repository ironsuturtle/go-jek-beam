package com.gojek.beam.interview.option;

import org.apache.beam.sdk.options.PipelineOptionsFactory;
import org.junit.Assert;
import org.junit.Test;

public class BookingOptionsTest {

    @Test
    public void testBookingOptions() {
        String[] curArgs = new String[2];
        curArgs[0] = "--inputFile=input.txt";
        curArgs[1] = "--output=output.txt";
        BookingOptions bookingOptions = PipelineOptionsFactory.fromArgs(curArgs).withValidation().as(BookingOptions.class);
        bookingOptions.setOutputType("bigQuery");
        bookingOptions.setWindowSize(60);
        Assert.assertEquals("input.txt", bookingOptions.getInputFile());
        Assert.assertEquals("output.txt", bookingOptions.getOutput());
        Assert.assertEquals("bigQuery", bookingOptions.getOutputType());
        Assert.assertEquals(new Integer(60), bookingOptions.getWindowSize());

    }
}
